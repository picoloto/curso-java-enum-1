package aplicacao;

import java.util.Date;

import entidades.Pedido;
import entidades.enums.Status;

public class Programa {

	public static void main(String[] args) {
		
		Pedido pedido = new Pedido(1080, new Date(), Status.AGUARDANDO_PAGAMENTO);
		
		System.out.println(pedido);
		
		Status s1 = Status.ENTREGUE;
		Status s2 = Status.valueOf("ENTREGUE");
		
		System.out.println(s1);
		System.out.println(s2);

	}

}
